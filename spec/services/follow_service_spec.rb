require 'rails_helper'

describe FollowService do
  let(:actor) { create(:account) }
  let(:target_object) { create(:account) }

  describe '#call' do
    let(:result) { described_class.new(actor, target_object).call }

    context 'when target_object is unlocked' do
      it 'creates follow' do
        expect { result }.to change(Follow.all, :count).by(1)

        last_follow = Follow.last
        expect(last_follow.follower).to eq actor
        expect(last_follow.following).to eq target_object
      end
    end

    context 'when target_object is locked' do
      let(:target_object) { create(:account, locked: true) }

      it 'creates follow request' do
        expect { result }.to change(FollowRequest.all, :count).by(1)

        last_follow_request = FollowRequest.last
        expect(last_follow_request.follower).to eq actor
        expect(last_follow_request.following).to eq target_object
      end
    end

    context 'when actor already follows target object' do
      before do
        create(:follow, follower: actor, following: target_object)
      end

      it 'does not create follow' do
        expect { result }.to_not change(Follow.all, :count)
      end
    end

    context 'when actor already requested follow of target object' do
      before do
        create(:follow_request, follower: actor, following: target_object)
      end

      it 'does not create follow' do
        expect { result }.to_not change(FollowRequest.all, :count)
      end
    end

    context 'when target_object is account and is not locked' do
      it 'creates new_follower notification' do
        expect(CreateNotificationJob)
          .to receive(:call)
          .with('new_follower', author: actor, recipient: target_object)

        result
      end
    end

    context 'when target_object is tag' do
      let(:target_object) { create(:tag) }

      it 'creates new_follower notification' do
        expect(CreateNotificationJob)
          .to_not receive(:call)

        result
      end
    end
  end
end
