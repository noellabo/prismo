# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ActivityPub::TagManager do
  include RoutingHelper

  subject { described_class.instance }

  describe '#url_for' do
    it 'returns a string' do
      account = create(:account)
      expect(subject.url_for(account)).to be_a String
    end
  end

  describe '#uri_for' do
    it 'returns a string' do
      account = create(:account)
      expect(subject.uri_for(account)).to be_a String
    end
  end

  describe '#to' do
    it 'returns public collection for public status' do
      story = create(:activitypub_post)
      expect(subject.to(story))
        .to eq ['https://www.w3.org/ns/activitystreams#Public']
    end
  end

  xdescribe '#cc' do
    it 'returns followers collection for public story' do
      story = create(:activitypub_post)
      expect(subject.cc(story)).to eq [account_followers_url(story.account)]
    end
  end

  describe '#local_uri?' do
    it 'returns false for non-local URI' do
      expect(subject.local_uri?('http://example.com/123')).to be false
    end

    it 'returns true for local URIs' do
      account = create(:account, domain: nil)
      uri = subject.uri_for(account)
      expect(subject.local_uri?(uri)).to be true
    end
  end

  describe '#uri_to_local_id' do
    it 'returns the local ID' do
      account = create(:account)
      expect(subject.uri_to_local_id(subject.uri_for(account), :username))
        .to eq account.username
    end
  end

  describe '#uri_to_resource' do
    it 'returns the local account' do
      account = create(:account)
      expect(subject.uri_to_resource(subject.uri_for(account), Account))
        .to eq account
    end

    it 'returns the remote account by matching URI without fragment part' do
      account = create(:account, uri: 'https://example.com/123')
      expect(subject.uri_to_resource('https://example.com/123#456', Account))
        .to eq account
    end

    it 'returns the local post for ActivityPub URI' do
      post = create(:activitypub_post)
      expect(subject.uri_to_resource(subject.uri_for(post), ActivityPubPost)).to eq post
    end

    it 'returns the remote story by matching URI without fragment part' do
      story = create(:activitypub_post, uri: 'https://example.com/123')
      expect(subject.uri_to_resource('https://example.com/123#456', ActivityPubPost))
        .to eq story
    end
  end
end
