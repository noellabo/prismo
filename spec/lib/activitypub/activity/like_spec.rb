# frozen_string_literal: true

require 'rails_helper'

describe ActivityPub::Activity::Like do
  let(:sender)    { create(:account) }
  let(:recipient) { create(:account) }
  let(:story)     { create(:activitypub_post, account: recipient) }
  let(:comment)   { create(:activitypub_comment) }

  let(:story_json) do
    {
      '@context': 'https://www.w3.org/ns/activitystreams',
      id: 'foo',
      type: 'Like',
      actor: ActivityPub::TagManager.instance.uri_for(sender),
      object: ActivityPub::TagManager.instance.uri_for(story)
    }.with_indifferent_access
  end

  let(:comment_json) do
    {
      '@context': 'https://www.w3.org/ns/activitystreams',
      id: 'foo',
      type: 'Like',
      actor: ActivityPub::TagManager.instance.uri_for(sender),
      object: ActivityPub::TagManager.instance.uri_for(comment)
    }.with_indifferent_access
  end

  describe '#perform' do
    subject { described_class.new(json, sender) }

    context 'when resource is a story' do
      let(:json) { story_json }

      it 'creates a favourite from sender to story' do
        expect { subject.perform }
          .to change { sender.liked?(story) }
          .from(false).to(true)
      end
    end

    context 'when resource is a comment' do
      let(:json) { comment_json }

      it 'creates a favourite from sender to story' do
        expect { subject.perform }
          .to change { sender.liked?(comment) }
          .from(false).to(true)
      end
    end
  end
end
