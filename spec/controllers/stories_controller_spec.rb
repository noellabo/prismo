# frozen_string_literal: true

require 'rails_helper'

describe StoriesController, type: :controller do
  render_views

  let(:story) { create(:activitypub_post) }

  describe 'GET #index' do
    context 'when requesting html' do
      subject { get :index, format: :html }

      it 'renders successfull response' do
        subject
        expect(response).to be_successful
        expect(response.content_type).to eq 'text/html'
      end
    end

    context 'when requesting atom' do
      subject { get :index, format: :atom }

      it 'renders successfull response' do
        subject
        expect(response).to be_successful
        expect(response.content_type).to eq 'application/atom+xml'
      end
    end
  end

  describe 'GET #recent' do
    context 'when requesting html' do
      subject { get :recent, format: :html }

      it 'renders successfull response' do
        subject
        expect(response).to be_successful
        expect(response.content_type).to eq 'text/html'
      end
    end

    context 'when requesting atom' do
      subject { get :recent, format: :atom }

      it 'renders successfull response' do
        subject
        expect(response).to be_successful
        expect(response.content_type).to eq 'application/atom+xml'
      end
    end
  end

  describe 'GET #show' do
    subject { get :show, params: { id: story.id } }

    it 'renders successfull response' do
      subject
      expect(response).to be_successful
    end
  end
end
