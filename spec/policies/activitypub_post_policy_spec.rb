# frozen_string_literal: true

require 'rails_helper'

describe ActivityPubPostPolicy do
  subject { described_class }

  let(:user) { create(:user, :with_account) }
  let(:user_silenced) { create(:user, :with_account) }
  let(:admin) { create(:user, :admin, :with_account) }
  let(:account) { user.account }
  let(:story) { create(:activitypub_post, account: account) }

  before do
    user_silenced.account.update(silenced: true)
  end

  shared_examples 'is prohibited for guests' do
    it { expect(subject).to_not permit(nil, story) }
  end

  shared_examples 'is available for guests' do
    it { expect(subject).to permit(nil, story) }
  end

  shared_examples 'is available for any non-guest user' do
    it { expect(subject).to permit(create(:user, :with_account), story) }
  end

  shared_examples 'is prohibitet for user other than story author' do
    it { expect(subject).not_to permit(create(:user, :with_account), story) }
  end

  shared_examples 'is available for story author' do
    it { expect(subject).to permit(story.account.user, story) }
  end

  shared_examples 'is available for admin' do
    it { expect(subject).to permit(admin, story) }
  end

  shared_examples 'is prohibitet for silenced user' do
    it { expect(subject).to_not permit(user_silenced, story) }
  end

  permissions :index? do
    it_behaves_like 'is available for guests'
    it_behaves_like 'is available for any non-guest user'
  end

  permissions :create? do
    it_behaves_like 'is prohibited for guests'
    it_behaves_like 'is available for any non-guest user'
    it_behaves_like 'is prohibitet for silenced user'
  end

  permissions :update? do
    it_behaves_like 'is prohibitet for user other than story author'
    it_behaves_like 'is available for story author'
    it_behaves_like 'is available for admin'
  end

  permissions :scrap? do
    it_behaves_like 'is prohibitet for user other than story author'
    it_behaves_like 'is available for story author'
    it_behaves_like 'is available for admin'
  end

  permissions :comment? do
    it_behaves_like 'is prohibited for guests'
    it_behaves_like 'is available for any non-guest user'
    it_behaves_like 'is prohibitet for silenced user'
  end

  permissions :toggle_like? do
    it_behaves_like 'is prohibited for guests'
    it_behaves_like 'is available for any non-guest user'

    it 'is prohibited for removed stories' do
      expect(subject)
        .to_not permit(create(:user, :with_account), create(:activitypub_post, :removed))
    end
  end

  permissions :update_title? do
    it_behaves_like 'is available for admin'

    it 'is available for author when post is younger than limit allows' do
      post = build(:story, created_at: 59.minutes.ago, account: account)

      expect(subject).to permit(user, post)
    end

    it 'is prohibited for author when post is older than limit allows' do
      post = build(:story, created_at: 61.minutes.ago, account: account)

      expect(subject).to_not permit(user, post)
    end

    it 'is available for admin even when post is older than limit allows' do
      post = build(:story, created_at: 70.minutes.ago, account: account)

      expect(subject).to permit(admin, post)
    end
  end
end
