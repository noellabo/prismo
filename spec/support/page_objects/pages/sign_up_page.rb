# frozen_string_literal: true

class SignUpPage < SitePrism::Page
  set_url '/auth/sign_up'
  set_url_matcher %r{\/auth(\/sign_up)?\z}

  element :username_field, 'input#user_account_attributes_username'
  element :email_field, 'input#user_email'
  element :password_field, 'input#user_password'
  element :password_confirmation_field, 'input#user_password_confirmation'
  element :submit_button, '[type="submit"]'
end
