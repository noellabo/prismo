# frozen_string_literal: true

require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
require 'sprockets/railtie'
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

require_relative '../lib/prismo/version'
require_relative '../lib/redcarpet/render/account_bio'

module Prismo
  class Application < Rails::Application
    port     = ENV.fetch('PORT') { 3000 }
    host     = ENV.fetch('LOCAL_DOMAIN') { "localhost:#{port}" }
    web_host = ENV.fetch('WEB_DOMAIN') { host }
    https    = Rails.env.production? || ENV['LOCAL_HTTPS'] == 'true'

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    config.active_record.schema_format = :sql

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # All translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.available_locales = [
      :en,
      :ja,
      :pl,
    ]

    config.i18n.default_locale = ENV['DEFAULT_LOCALE']&.to_sym

    unless config.i18n.available_locales.include?(config.i18n.default_locale)
      config.i18n.default_locale = :en
    end

    # Don't generate system test files.
    config.generators.system_tests = nil

    config.active_job.queue_adapter = :sidekiq

    config.x.local_domain = host
    config.x.web_domain = web_host

    config.x.uuid_regexp = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/

    config.action_mailer.default_url_options = {
      host: web_host,
      protocol: https ? 'https://' : 'http://',
      trailing_slash: false
    }

    config.action_controller.asset_host = [(https ? 'https' : 'http'), web_host].join('://')

    config.to_prepare do
      Devise::SessionsController.layout 'auth'
      Devise::RegistrationsController.layout 'auth'
      Devise::PasswordsController.layout 'auth'
      Devise::ConfirmationsController.layout 'auth'
    end
  end
end
