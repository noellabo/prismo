# frozen_string_literal: true

class RemovedCommentNull
  attr_accessor :comment, :parent_id

  def initialize(comment = ActivityPubComment.new)
    @comment = comment.decorate
  end

  def account
    Account.new
  end

  def body
    'Comment removed'
  end

  def body_html
    '<p>Removed</p>'
  end

  def decorate
    ActivityPubCommentDecorator.new(self)
  end

  delegate :id,
           :story,
           :story_id,
           :children_count,
           :likes_count,
           :created_at,
           :to_model,
           :removed?,
           :children,
           :object_type,
           to: :comment

  class Account
    def id
      nil
    end

    def avatar_url(size = :size_60)
      '/placeholders/avatar.jpg'
    end

    def path
      nil
    end

    def decorate
      self
    end

    def username
      'Ghost'
    end

    def username_with_at
      username
    end

    def silenced?
      false
    end

    def suspended?
      false
    end
  end
end
