# frozen_string_literal: true

class Jumpbox
  include Singleton

  class Link
    attr_reader :label, :path, :media, :auth_required, :opts, :link_opts

    def initialize(label, path, media, opts = {})
      @label = label
      @path = path
      @media = media
      @opts = opts.slice(:auth_required)
      @link_opts = opts.slice(:active)
    end

    def to_s
      label
    end
  end

  class IconLink < Link
    def media_type
      :icon
    end

    def media_class
      "fe fe-#{media}"
    end
  end

  class AvatarLink < Link
    def media_type
      :avatar
    end
  end

  STORIES_LINK = IconLink.new('Stories', :stories, 'link-2', active: { controller: :stories })
  COMMENTS_LINK = IconLink.new('Comments', :comments, 'message-circle')
  DASHBOARD_LINK = IconLink.new('Dashboard', :dashboard, 'home', auth_required: true, active: { controller: :dashboards })
  NOTIFICATIONS_LINK = IconLink.new('Notifications', :notifications, 'bell')
  DEFAULT_LINK = STORIES_LINK

  attr_reader :prismo_feeds

  def initialize
    @prismo_feeds = []

    # Fill prismo feeds collection
    @prismo_feeds << DASHBOARD_LINK
    @prismo_feeds << STORIES_LINK
    @prismo_feeds << COMMENTS_LINK
  end

  def resolve(resource)
    case resource
    when IconLink, AvatarLink
      resource
    when Account
      AvatarLink.new(resource.to_s,
                     resource.path,
                     resource.avatar_url(:size_60))
    when Gutentag::Tag
      IconLink.new(resource.name,
                   resource.path,
                   :hash)
    else
      raise 'Unregistered resource type'
    end
  end
end
