# frozen_string_literal: true

class ActivityPub::StorySerializer < ActivityPub::BaseSerializer
  def data
    {
      id: ActivityPub::TagManager.instance.uri_for(object),
      type: object.object_type.to_s.capitalize,
      name: object.name,
      summary: object.decorate.description_excerpt&.chomp,
      published: object.created_at.iso8601,
      content: object.content,
      source: source,
      url: url,
      likes: object.likes_count,
      attributedTo: attributed_to,
      to: [ActivityPub::TagManager::COLLECTIONS[:public]],
      tag: tags
    }
  end

  private

  def url
    {
      type: 'Link',
      mediaType: 'text/html',
      href: object.article? ? story_url(object) : object.url
    }
  end

  def attributed_to
    [
      { type: 'Person', id: ActivityPub::TagManager.instance.uri_for(object.account) }
    ]
  end

  def tags
    TagSerializer.list(object.tags).data
  end

  def source
    {
      content: object.content_source,
      mediaType: 'text/markdown'
    }
  end

  class TagSerializer < ActivityPub::BaseSerializer
    def data
      {
        type: 'Hashtag',
        href: tag_stories_url(object.name),
        name: "##{object.name}"
      }
    end
  end
end
