# frozen_string_literal: true

class ActivityPub::CommentSerializer < ActivityPub::BaseSerializer
  def data
    {
      id: ActivityPub::TagManager.instance.uri_for(object),
      type: object.object_type.to_s.capitalize,
      published: object.created_at.iso8601,
      url: url,
      likes: object.likes_count,
      attributedTo: attributed_to,
      to: [ActivityPub::TagManager::COLLECTIONS[:public]],
      inReplyTo: in_reply_to,
      content: object.content,
      source: source
    }
  end

  def url
    { type: 'Link', mediaType: 'text/html', href: comment_url(object) }
  end

  def attributed_to
    [
      { type: 'Person', id: account_url(object.account.username) }
    ]
  end

  def in_reply_to
    if object.parent_id.present?
      ActivityPub::TagManager.instance.uri_for(object.parent)
    else
      ActivityPub::TagManager.instance.uri_for(object.story)
    end
  end

  def source
    {
      content: object.content_source,
      mediaType: 'text/markdown'
    }
  end
end
