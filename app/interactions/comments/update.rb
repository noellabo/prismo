# frozen_string_literal: true

module Comments
  class Update < ActiveInteraction::Base
    object :comment, class: ActivityPubComment
    string :body

    def execute
      comment.content_source = body if body?

      comment.modified_at = Time.current
      comment.modified_count += 1 if edit_grace_period_passed?

      if comment.save
        after_comment_save_hook(comment)
      else
        errors.merge!(comment.errors)
      end

      comment
    end

    def persisted?
      true
    end

    private

    def after_comment_save_hook(comment)
      comment.cache_body
      Comments::BroadcastChanges.run!(comment: comment)
      ActivityPub::UpdateDistributionJob.call_later(comment) if comment.local?
    end

    def edit_grace_period_passed?
      period = Setting.edit_counter_grace_period_minutes
      period.to_i.minutes.ago > comment.created_at
    end
  end
end
