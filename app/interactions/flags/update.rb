# frozen_string_literal: true

class Flags::Update < ActiveInteraction::Base
  object :flag
  string :summary

  def execute
    flag.summary = summary if summary?

    if flag.valid?
      flag.save
    else
      errors.merge!(flag.errors)
    end

    flag
  end

  def persisted?
    true
  end
end
