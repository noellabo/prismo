# frozen_string_literal: true

class Comments::DeleteJob < ApplicationJob
  queue_as :default

  def perform(comment_id)
    Comments::Delete.run!(comment: ActivityPubComment.find(comment_id))
  end
end
