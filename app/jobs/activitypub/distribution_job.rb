# frozen_string_literal: true

class ActivityPub::DistributionJob < ApplicationJob
  queue_as :push

  def self.call(resource)
    ActivityPub::DistributionJob.perform_later(resource.class.name, resource.id)
  end

  def perform(resource_type, resource_id)
    @resource = resource_type.constantize.find(resource_id)
    @account = @resource.account

    inboxes.each do |inbox_url|
      ActivityPub::DeliveryJob.perform_later(
        signed_payload, @account.id, inbox_url
      )
    end

  rescue ActiveRecord::RecordNotFound
    true
  end

  private

  def inboxes
    case @resource
    when ActivityPubComment
      @resource.root.comments.remote.map { |c| c.account.inbox_url }.reject(&:blank?)
    when ActivityPubPost
      @resource.account.followers.inboxes.reject(&:blank?)
    else
      []
    end
  end

  def signed_payload
    Oj.dump(ActivityPub::LinkedDataSignature.new(payload).sign!(@account))
  end

  def payload
    ActivityPub::ActivitySerializer.new(@resource, with_context: true).as_json
  end
end
