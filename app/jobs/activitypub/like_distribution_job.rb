# frozen_string_literal: true

class ActivityPub::LikeDistributionJob < ApplicationJob
  queue_as :push

  def perform(like_id)
    @like = Like.find(like_id)
    @likeable = like.likeable
    @account = like.account

    inboxes.each do |inbox_url|
      ActivityPub::DeliveryJob.perform_later(
        signed_payload, account.id, inbox_url
      )
    end

  rescue ActiveRecord::RecordNotFound
    true
  end

  private

  attr_reader :like, :account, :likeable

  def inboxes
    if likeable.is_a?(ActivityPubComment)
      [likeable.account.inbox_url].reject(&:blank?)
    else
      []
    end
  end

  def signed_payload
    @signed_payload ||= Oj.dump(ActivityPub::LinkedDataSignature.new(payload).sign!(account))
  end

  def payload
    @payload ||= ActivityPub::LikeSerializer.new(likeable, with_context: true).as_json
  end
end
