# frozen_string_literal: true

class ActivityPub::UpdateDistributionJob < ApplicationJob
  queue_as :push

  def self.call_later(resource)
    perform_later(resource.id, resource.class.name)
  end

  def perform(resource_id, resource_class_name)
    @resource = resource_class_name.constantize.find(resource_id)

    inboxes.each do |inbox_url|
      ActivityPub::DeliveryJob.perform_later(signed_payload, account.id, inbox_url)
    end
  rescue ActiveRecord::RecordNotFound
    true
  end

  private

  attr_reader :resource

  def account
    @account ||= resource.is_a?(Account) ? resource : resource.account
  end

  def inboxes
    @inboxes ||= account.followers.inboxes.reject(&:blank?)
  end

  def signed_payload
    @signed_payload ||= Oj.dump(ActivityPub::LinkedDataSignature.new(payload).sign!(account))
  end

  def payload
    ActivityPub::UpdateSerializer.new(resource, with_context: true).as_json
  end
end
