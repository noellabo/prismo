import ActionCable from 'actioncable'

const localHttpsEl = document.querySelector("meta[name=local-https]")
const localDomainEl = document.querySelector("meta[name=local-domain]")

let https = process.env.RAILS_ENV == 'production' || localHttpsEl.content == 'true'
let protocol = https ? 'wss' : 'ws'

const ws = ActionCable.createConsumer(`${protocol}://${window.location.host}/cable`)

export default ws
