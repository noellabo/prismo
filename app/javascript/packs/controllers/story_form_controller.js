import BaseController from './base_controller'
import axios from 'axios'
import Tagify from '@yaireo/tagify'
import api from '../lib/api'
import { debounce } from "debounce"

export default class extends BaseController {
  static targets = ['urlInput', 'titleInput', 'fetchTitleBtn', 'tagsInput', 'tagsPhantomInput']

  connect () {
    this._initTags()
  }

  fetchTitle (e) {
    e.preventDefault()

    if (this.fetchTitleBtnDisabled) return false
    this.loading = true

    let req = axios.post('/api/v1/tools/scrap_url', {
      url: this.url
    })

    req.then((resp) => {
      this.title = resp.data.title
      this.url = resp.data.url
      this.loading = false
    })

    req.catch(() => {
      this.loading = false
    })
  }

  handleUrlChange () {
    this.urlDisabled = this.urlInputTarget.value.length == 0
  }

  _initTags () {
    let _this = this

    this.tagify = new Tagify(this.tagsPhantomInputTarget, {
      delimiters: ', ',
      maxTags: this.tagsPhantomInputTarget.dataset.maxTags,
      transformTag (tagData) { return tagData.value.replace('#', '') }
    })

    this.tagify.on('add', () => { this._updateTagsInput() })
    this.tagify.on('remove', () => { this._updateTagsInput() })

    let fetchTags = debounce((e) => { this._fetchTags(e) }, 300)
    this.tagify.on('input', (e) => fetchTags(e))
  }

  _updateTagsInput () {
    let val = this.tagify.value.map((tag) => { return tag.value }).join(',')
    this.tagsList = val
  }

  _fetchTags (e) {
    let value = e.detail.value

    this.tagify.settings.whitelist.length = 0
    let req = api.tags({ q: value })

    req.then((resp) => {
      this.tagify.settings.whitelist = resp.data.map((tag) => { return tag.name })
      this.tagify.dropdown.show.call(this.tagify, value)
    })
  }

  get url () {
    return this.urlInputTarget.value
  }

  get fetchTitleBtnDisabled () {
    return this.fetchTitleBtnTarget.classList.contains('btn--disabled')
  }

  set title (title) {
    this.titleInputTarget.value = title
  }

  set url (url) {
    this.urlInputTarget.value = url
  }

  set urlDisabled (disable) {
    disable ?
      this.fetchTitleBtnTarget.classList.add('btn--disabled') :
      this.fetchTitleBtnTarget.classList.remove('btn--disabled')
  }

  set tagsList (value) {
    this.tagsInputTarget.setAttribute('value', value)
  }

  set loading (value) {
    value ?
      this.fetchTitleBtnTarget.classList.add('btn--loading') :
      this.fetchTitleBtnTarget.classList.remove('btn--loading')
  }
}
