# frozen_string_literal: true

class ActivityPub::AccountsController < ActivityPub::BaseController
  include RoutingHelper

  before_action :set_account
  before_action :set_stories

  def show
    account = find_account
    render json: ActivityPub::ActorSerializer.new(account),
           with_context: true,
           content_type: 'application/activity+json'
  end

  def outbox
    render json: ActivityPub::OutboxSerializer.new(outbox_presenter),
           with_context: true,
           content_type: 'application/activity+json'
  end

  private

  def find_account
    Account.find_by(username: params[:username])
  end

  def outbox_presenter
    if page_requested?
      ActivityPub::CollectionDecorator.new(
        id: outbox_activitypub_account_url(@account, page_params),
        type: :ordered,
        part_of: outbox_activitypub_account_url(@account),
        prev: prev_page,
        next: next_page,
        items: @stories
      )
    else
      ActivityPub::CollectionDecorator.new(
        id: outbox_activitypub_account_url(@account),
        type: :ordered,
        size: @account.stories_count,
        first: outbox_activitypub_account_url(@account, paged: true),
        last: outbox_activitypub_account_url(@account, paged: true, page: last_page_number)
      )
    end
  end

  def set_account
    @account = ::Account.find_by(username: params[:username])
  end

  def set_stories
    return unless page_requested?

    @stories = @account.stories.page(params[:page])
  end

  def next_page
    outbox_activitypub_account_url(@account, paged: true, page: @stories.next_page) if @stories.next_page
  end

  def prev_page
    outbox_activitypub_account_url(@account, paged: true, page: @stories.prev_page) if @stories.prev_page
  end

  def page_requested?
    params[:paged] == 'true'
  end

  def last_page_number
    stories_count = @account.stories_count
    stories_count >= 25 ? (@account.stories_count / 25).to_i : nil
  end

  def page_params
    { paged: true, page: params[:page] }.compact
  end
end
