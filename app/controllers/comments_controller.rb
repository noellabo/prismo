# frozen_string_literal: true

class CommentsController < ApplicationController
  layout 'application'

  before_action :set_account_liked_comment_ids

  before_action { set_jumpbox_link(Jumpbox::COMMENTS_LINK) }

  def index
    comments = CommentsQuery.new.hot
    comments = CommentsQuery.new(comments).with_story
    @comments = comments.page(params[:page])
  end

  def recent
    comments = CommentsQuery.new.recent
    comments = CommentsQuery.new(comments).with_story
    @comments = comments.page(params[:page])
    render :index
  end

  def show
    set_account_liked_story_ids

    comment = find_comment
    @comment = comment.removed? ? RemovedCommentNull.new(comment) : comment
    @children = @comment.children.includes(:account).hash_tree

    respond_to do |format|
      format.html
      format.json do
        render json: ActivityPub::CommentSerializer.new(@comment),
               with_context: true,
               content_type: 'application/activity+json'
      end
    end
  end

  private

  def find_comment
    ActivityPubComment.find(params[:id])
  end
end
