# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Localized
  include Pundit
  include HasJumpbox

  helper_method :current_account

  before_action :detect_device_format
  before_action :suspension_check!

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def current_account
    current_user&.account&.decorate
  end

  def set_account_liked_comment_ids
    ids = user_signed_in? ? current_account.liked_comments.map(&:likeable_id) : []
    @account_liked_comment_ids = ids
  end

  def set_account_liked_story_ids
    ids = user_signed_in? ? current_account.liked_stories.map(&:likeable_id) : []
    @account_liked_story_ids = ids
  end

  def detect_device_format
    case request.user_agent
    when /iPhone/i
      request.variant = :phone
    when /Android/i && /mobile/i
      request.variant = :phone
    when /Windows Phone/i
      request.variant = :phone
      end
  end

  def user_not_authorized
    flash[:alert] = I18n.t('generic.not_authorized')
    redirect_to(request.referrer || root_path)
  end

  def suspension_check!
    suspended if user_signed_in? && current_user.suspended?
  end

  protected

  def suspended
    render plain: 'Your account has been suspended', status: :forbidden
  end
end
