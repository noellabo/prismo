# frozen_string_literal: true

atom_feed do |feed|
  feed.title(@feed_title)
  feed.updated(@stories[0].created_at) if @stories.any?
  feed.icon(root_url + 'favicon.ico')
  feed.generator('△ Prismo', version: Prismo::Version.to_s)

  @stories.each do |story|
    feed.entry(story, :url => story.url) do |entry|
      render('stories/story', story: story, entry: entry)
    end
  end
end
