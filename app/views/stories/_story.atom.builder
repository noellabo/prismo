# frozen_string_literal: true

entry.title(story.name)
entry.summary(story.decorate.excerpt)

entry.author do |author|
  decorated_author = story.account.decorate

  author.name(decorated_author)
  author.uri(decorated_author.profile_url)
end

story.tags.each do |tag|
  entry.category(term: tag.name, label: tag.name)
end
