# frozen_string_literal: true

group = Group.create!(
  name: 'General',
  supergroup: true
)

account = Account.create!(
  username: 'admin',
  display_name: 'Site Admin'
)

User.create!(
  account: account,
  email: 'admin@example.com',
  password: 'TestPass',
  is_admin: true,
  confirmed_at: Time.zone.now
)

stories = [
  {
    account: account,
    name: 'Szokujące słowa minister: Facebook nie zapłacił w Polsce ani grosza podatku',
    url: 'http://m.superbiz.se.pl/wiadomosci-biz/szokujace-slowa-minister-facebook-nie-zaplacil-w-polsce-ani-gorsza-podatku_1055497.html',
    url_domain: 'm.suberbiz.se.pl',
    likes_count: 14,
    tag_names: ['seo', 'security'],
    group: group

  }, {
    account: account,
    name: 'GDPR Hall of Shame',
    url: 'http://gdprhallofshame.com/',
    url_domain: 'gdprhallofshame.com',
    likes_count: 51,
    tag_names: ['security'],
    group: group
  }, {
    account: account,
    name: 'Pony 0.22.0 Released',
    url: 'https://www.ponylang.org/blog/2018/05/0.22.0-released/',
    url_domain: 'www.ponylang.org',
    likes_count: 6,
    tag_names: ['release'],
    group: group
  }, {
    account: account,
    name: 'WireGuard is available for OpenBSD',
    url: 'https://marc.info/?l=openbsd-ports&m=152712417729497&w=2',
    url_domain: 'marc.info',
    likes_count: 31,
    tag_names: ['networking', 'openbsd', 'security'],
    group: group
  }
]

ActivityPubPost.create!(stories)

story = ActivityPubPost.first

comment1 = story.comments.create! account: account, content_source: 'Sample comment 1'
comment1_1 = ActivityPubComment.create! parent: comment1, account: account, content_source: 'Sample comment 1-1'
comment1_2 = ActivityPubComment.create! parent: comment1, account: account, content_source: 'Sample comment 1-2'

comment2 = story.comments.create! account: account, content_source: 'Sample comment 2'
comment2_1 = ActivityPubComment.create! parent: comment2, account: account, content_source: 'Sample comment 2-1'
comment2_2 = ActivityPubComment.create! parent: comment2, account: account, content_source: 'Sample comment 2-2'
comment2_2_1 = ActivityPubComment.create! parent: comment2, account: account, content_source: 'Sample comment 2-2-1'

ActivityPubComment.all.each do |comment|
  comment.cache_depth
  comment.cache_body
  comment.cache_root
end
