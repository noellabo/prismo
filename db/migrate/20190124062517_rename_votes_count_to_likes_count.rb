class RenameVotesCountToLikesCount < ActiveRecord::Migration[5.2]
  def change
    rename_column :stories, :votes_count, :likes_count
    rename_column :comments, :votes_count, :likes_count
  end
end
