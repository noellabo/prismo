class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.string :name
      t.string :slug
      t.string :domain
      t.boolean :supergroup, default: false

      t.index ["slug", "domain"], name: "index_groups_on_slug_and_domain", unique: true

      t.timestamps
    end
  end
end
